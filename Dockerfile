FROM python:3.8-slim

LABEL name="rastasteady/rastasteady" \
      version="latest" \
      description="RastaSteady es un software de estabilizacion de video para el sistema DJI FPV digital."

RUN mkdir -p /opt/rastasteady
ADD . /opt/rastasteady
RUN pip3 install -e /opt/rastasteady

RUN apt-get update && \
    apt-get install -y wget xz-utils && \
    wget https://johnvansickle.com/ffmpeg/releases/ffmpeg-release-amd64-static.tar.xz && \
    tar -xJf ffmpeg-release-amd64-static.tar.xz -C /usr/local/bin --strip 1 && \
    rm -fr ffmpeg-release-amd64-static.tar.xz && \
    rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["/usr/local/bin/rastasteady"]
CMD ["-h"]
