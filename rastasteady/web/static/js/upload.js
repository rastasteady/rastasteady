var uuid = '';

function _(el) {
  return document.getElementById(el);
}

function uploadFile() {
  // captures the file from the form data
  var file = _('file').files[0];
  var formdata = new FormData();
  formdata.append('file', file);

  // hides the form and shows the progress components
  $('#upload_form').hide();
  $('#progressBar').show();
  $('#loaded_n_total').show();

  // uploads the file
  var ajax = new XMLHttpRequest();
  ajax.upload.addEventListener('progress', progressHandler, false);
  ajax.addEventListener('load', completeHandler, false);
  ajax.addEventListener('error', errorHandler, false);
  ajax.addEventListener('abort', abortHandler, false);
  ajax.open('POST', '/api/upload', true);
  ajax.send(formdata);
}

function progressHandler(event) {
  // update progress bar
  var percent = (event.loaded / event.total) * 100;
  _('progressBar').value = Math.round(percent);
  _('status').innerHTML = Math.round(percent) + '% completado';
}

function completeHandler(event) {
  data = JSON.parse(event.target.responseText);

  // update the status message
  _('status').innerHTML = data['msg'];

  // hide progress bar
  $('#loaded_n_total').hide();
  $('#progressBar').hide();

  if (data['result'] == 'ok' ) {
    // capture the uuid of the uploaded file
    uuid = data['uuid'];

    // update the queue size
    _('queue_status').innerHTML = 'Actualmente hay ' + data['working'] + ' videos siendo procesados y ' + data['pending'] + ' en cola';

    // hide the original title
    $('#title').hide();
  }
  else {
    // if there's an error show the form again
    $('#upload_form').show();
  }
}

function errorHandler(event) {
  _('status').innerHTML = 'Error: Comprueba el formato y el tamaño máximo';

  // update the queue size
  _('queue_status').innerHTML = 'Actualmente hay ' + data['working'] + ' videos siendo procesados y ' + data['pending'] + ' en cola';

  // hide progress bar
  $('#loaded_n_total').hide();
  $('#progressBar').hide();

  $('#title').show();
  $('#upload_form').show();
}

function abortHandler(event) {
  _('status').innerHTML = 'Error: Subida abortada';

  // update the queue size
  _('queue_status').innerHTML = 'Actualmente hay ' + data['working'] + ' videos siendo procesados y ' + data['pending'] + ' en cola';

  // hide progress bar
  $('#loaded_n_total').hide();
  $('#progressBar').hide();

  $('#title').show();
  $('#upload_form').show();
}

function refreshStatus() {
  $.get('/api/process', function(data) {
    _('queue_status').innerHTML = 'Actualmente hay ' + data['working'] + ' videos siendo procesados y ' + data['pending'] + ' en cola';
  });
  setTimeout(refreshStatus, 10000);
};
refreshStatus();
